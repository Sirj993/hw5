<?php
$phoneNum = "tel:+380631234567";

$mail = [
    "mailto:notinfo@site.com",
    'Имя_сайта@кек.com',
];

$address = 'г. Одесса, улица 5';
?>














<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hw5</title>
    <meta name="description" content="Hw5">
    <link rel="stylesheet" href="/styles.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>
<body>
    <!--header-->
    <div class="header">
    <!--links-->
    <div class="links">
        <ul>
            <li>
                <a href="index.php">Курс Валют</a>
            </li>
            <li>
                <a href="/calculator.php">Калькулятор валют</a>
            </li>
            <li>
                <a href="/contacts.php">Контакты</a>
            </li>
        </ul>
    </div>
    <h2 class="textCenter">Вы находитесь на название сайта</h2>
    </div>
    <!--body-->

    <div class="wrp contacts">
		<div class="map-box">
			<h4>Наши контакты</h4>
                <!--different variations-->
            <?= $address?>
			<p><a href="<?= $phoneNum?>">+380ХХХХХХХХХ</a></p>            
			<p><a href="<?= $mail[0]?>"><?= $mail[1]?></a></p>
		</div>
	</div>

        <div class="col-12 contacts">
            <h4>Свяжитесь с нами</h4>
            <form action="">
                <div class="mb-2">
                    <label class="form-label" for="first_name">Ваше имя:</label>
                    <input  class="form-control" id='first_name' type="text">
                </div>
                
                <div class="mb-2">
                    <label class="form-label" for="email">Ваша почта:</label>
                    <input  class="form-control" id='email' type="text">
                </div>
                
                <div class="mb-2">
                    <label class="form-label" for="message">Ваше сообщение:</label>
                    <textarea  class="form-control" id="message"></textarea>
                </div>
                
                <div class="mb-2">
                    <button class="btn btn-primary">Отправить</button>
                </div>
            </form>
        </div>





  <!--footer-->
  <div class="footer">
          <span>Название сайта © 2019</span>
  </div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>
</html>