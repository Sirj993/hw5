<?php
$usdName = 'USD';
$usdValuePurchase = 27.8426;
$usdValueSale = 28.0513;
$usdValueNbu = 27.0513;
$eurName = 'EUR';
$eurValuePurchase = 32.6575;
$eurValueSale = 33.0490;
$eurValueNbu = 32.7003;
$eur = [
    'EUR',
    32.6575,
    // как показать число полным, с 0 вконце, при этом оставить переменную флоат? 
    33.0490,
    32.7003,
];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hw5</title>
    <meta name="description" content="Hw5">
    <link rel="stylesheet" href="/styles.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>
<body>
    <!--header-->
    <div class="header">
            <!--links-->   
    <div class="links">
        <ul>
            <li>
                <a href="index.php">Курс Валют</a>
            </li>
            <li>
                <a href="/calculator.php">Калькулятор валют</a>
            </li>
            <li>
                <a href="/contacts.php">Контакты</a>
            </li>
        </ul>
    </div>
    <h2 class="textCenter">Вы находитесь на название сайта</h2>
    <H1 class="exRateHeader">
        Курс валют
    </H1>
    </div>
    <!--body-->
    <div class="body row justify-content-center">
        <div class="col-12">
            <table class="table table-hover">
                <tr>
                    <th>Валюта</th>
                    <th>Покупка</th>
                    <th>Продажа</th>
                    <th>НБУ</th>
                </tr>
                <tr>
                    <td> <?= $usdName?> </td>
                    <td> <?= $usdValuePurchase?> </td>
                    <td> <?= $usdValueSale?> </td>
                    <td> <?= $usdValueNbu?> </td>
                </tr>
                <tr>
                    <td><?=$eur[0]?></td>
                    <td><?=$eur[1]?></td>
                    <td><?=$eur[2]?></td>
                    <td><?=$eur[3]?></td>
                </tr>
            </table>
        </div>
    <!--footer-->
    <div class="footer">
        <H2 class="exRateHeader">Курс валют</H2>
        <span>Название сайта © 2019</span>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script></body>
</body>
</html>